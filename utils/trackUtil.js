const sa = require("../utils/sensorsdata.min.js");
let utm_content = "";
 
function setTrackConfig() {
  sa.setPara({
    appid: "wx21be13c4404fcac3",
    name: "sensors",
    // server_url: 'http://localhost:8080/api/bigData/ebizdemo',
    server_url: 'https://logapi.xiaoxiangai.com/gather-api/send',
    send_timeout: 1000,
    max_string_length: 500,
    use_client_time: false,
    show_log: true,
    allow_amend_share_path: true,
    is_persistent_save: true,
    autoTrack: {
      appLaunch: true, // 默认为 true，false 则关闭 $MPLaunch 事件采集
      appShow: true, // 默认为 true，false 则关闭 $MPShow 事件采集
      appHide: true, // 默认为 true，false 则关闭 $MPHide 事件采集
      pageShow: true, // 默认为 true，false 则关闭 $MPViewScreen 事件采集
      pageShare: true, // 默认为 true，false 则关闭 $MPShare 事件采集
      mpClick: true, // 默认为 false，true 则开启 $MPClick 事件采集 
      mpFavorite: true // 默认为 true，false 则关闭 $MPAddFavorites 事件采集
    },
    // 是否集成了插件！重要！
    is_plugin: true
  });
 
}

function initWithOpenId(openId) {
  const userId = wx.getStorageSync('user_id'); 
  userId && sa.login(userId);
  openId && sa.setOpenid(openId);
  sa.init();
}
 
function initWithOpenId(openId) {
  const userId = wx.getStorageSync('user_id'); 
  userId && sa.login(userId);
  openId && sa.setOpenid(openId);
  sa.init();
}
 
function auto_track_app_obj(query) {
  let res = {
    appName: "小象电商",
    appId: "wx21be13c4404fcac3",
    query: query
  };
  utm_content && (res.$utm_content = utm_content);
  return res;
}

function autoTrackAppLaunch(data) {
  sa.para.autoTrack.appLaunch = auto_track_app_obj(data.query);
}

function autoTrackAppShow(data) {
  sa.para.autoTrack.appShow = auto_track_app_obj(data.query);
}

function autoTrackAppHide(data) {
  sa.para.autoTrack.appHide = auto_track_app_obj(data.query);
}

module.exports = {
  setTrackConfig,
  initWithOpenId,
  autoTrackAppLaunch,
  autoTrackAppShow,
  autoTrackAppHide

};
